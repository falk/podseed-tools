<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us">
<head>
<title>podseed statistics</title>
<link href="http://podseed.org/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="http://podseed.org/css/podseed.css" rel="stylesheet" type="text/css">
<link href="theme.blue.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.10.4/themes/flick/jquery-ui.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="jquery-2.0.3.min.js"></script>

<script type="text/javascript" src="jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="jquery.tablesorter.widgets.min.js"></script>

<script type="text/javascript" src="jquery-ui-1.10.4.custom.min.js"></script>
<script type="text/javascript" src="highcharts.js"></script>
<script type="text/javascript" src="jquery.highchartTable-min.js"></script>

<script type="text/javascript">
$(function() {
$("#ep-details").tablesorter({theme:'blue', sortList: [[0,1]]});
$("#top-ua").tablesorter({theme:'blue', sortList: [[0,1]]});
$("#dp1").datepicker({
      showOtherMonths: true,
      selectOtherMonths: true
    });
$("#dp1").datepicker("option", "dateFormat", "yy-mm-dd");
$("#dp1").datepicker("setDate", "-1m");
$("#dp2").datepicker({
      showOtherMonths: true,
      selectOtherMonths: true
    });
$("#dp2").datepicker("option", "dateFormat", "yy-mm-dd");
$("#dp2").datepicker("setDate", "0");
});
</script>
</head>
<body>
<div class="container">
<?php 
$dbh = new mysqli("localhost", "podseed", "password_goes_here", "podseed");

if ( isset($_GET['podcast']) ) { $podcast = $dbh->real_escape_string($_GET['podcast']); } else { $podcast = ""; };
if ( isset($_GET['start']) ) { $start = $dbh->real_escape_string($_GET['start']); }
if ( isset($_GET['end']) ) { $end = $dbh->real_escape_string($_GET['end']); }

echo "<script type='text/javascript'>\n";
if ( isset($start) ) { echo "$( function(){ $('#dp1').datepicker('setDate','".$start."')});\n"; }
if ( isset($end) ) { echo  "$( function(){ $('#dp2').datepicker('setDate','".$end."')});\n"; }
echo "</script>";

echo "<p><form action='logs.php' method='GET'>";
echo "show <select name=podcast>";
$stm = "select distinct podcast from downloads order by podcast";
$res = $dbh->query($stm);
while ($row = $res->fetch_assoc()) {
  if ( $row['podcast'] == $podcast ) {
    echo "<option selected value='",$row['podcast'],"'>",$row['podcast'],"</option>";
  } else {
    echo "<option value='",$row['podcast'],"'>",$row['podcast'],"</option>";
  } 
  $available_podcasts[] = $row['podcast'];
}
echo "</select>";


echo " podcast from <input type=text name='start' id='dp1' size=10> to <input size=10 type=text name='end' id='dp2'>".
     " <input type='submit' value='Go'>&nbsp;<small>data is not live and lags one day behind :)</small></form></p>";

if ( $podcast && in_array($podcast, $available_podcasts) && $start && $end ) {
$stm = "select SUM(mp3_s+mp3_d+aac_s+aac_d+opus_d+opus_s) as total ".
       "from downloads where podcast='".$podcast."' and date between '".$start."' and '".$end."'";

$res = $dbh->query($stm);
$row = $res->fetch_assoc();

echo "<h3><font color=red>".$row['total']."</font> listens on <b>$podcast</b> from $start to $end</h3>";



$stm = "select episode, SUM(mp3_s+mp3_d) as mp3, SUM(aac_s+aac_d) as aac, SUM(opus_d+opus_s) as opus, ".
       "SUM(mp3_s+mp3_d+aac_s+aac_d+opus_s+opus_d) as total ".
         "from downloads where podcast='".$podcast."' and date between '".$start."' and '".$end."'".
         " group by episode order by total DESC limit 10";

$res = $dbh->query($stm);

echo "<h3>Top Ten Episodes</h3>";
echo "<div id='graph-top-ten'></div>";
echo "<table class='highchart' data-graph-type='column' style='display:none' data-graph-container='#graph-top-ten'><thead>".
     "<tr><th>Episode</th><th data-graph-stack-group='1'>MP3</th>".
     "<th data-graph-stack-group='1'>AAC</th><th data-graph-stack-group='1'>OPUS</th></tr></thead>";
echo "<tbody>";

while ($row = $res->fetch_assoc()) {
  echo "<tr><td>",$row['episode'],"</td><td>",$row['mp3'],"</td><td>",$row['aac'],"</td><td>",$row['opus'],"</td></tr>\n";
}
echo "</tbody></table>";
echo "<script type='text/javascript'>$(document).ready(function() {\n
  $('table.highchart').highchartTable();\n
});</script>";

echo "<h3>Listens per day</h3>";

$stm = "select date, SUM(mp3_s+mp3_d+aac_s+aac_d+opus_d+opus_s) as total ".
       "from downloads where podcast='".$podcast."' and date between '".$start."' and '".$end."' ".
       "group by date order by date ASC"; 

$res = $dbh->query($stm);

echo "<div id='graph-by-date'></div>";
echo "<table class='highchart' data-graph-type='line' data-graph-xaxis-type='datetime' style='display:none' data-graph-container='#graph-by-date'><thead>".
     "<tr><th>Date</th><th>Listens</th></tr></thead>";
echo "<tbody>";

while ($row = $res->fetch_assoc()) {
  echo "<tr><td>",$row['date'],"</td><td>",$row['total'],"</td></tr>\n";
}
echo "</tbody></table>";

echo "<h3>Top 20 Useragents</h3>";

$stm = "select useragent, SUM(count) as total ".
       "from useragents where podcast='".$podcast."' and date between '".$start."' and '".$end."' ".
       "group by useragent order by total DESC limit 20"; 


$res = $dbh->query($stm);
echo "<div class='row'>";
echo "<div id='top-ua' class='col-md-6'></div>";
echo "<div class='col-md-6'><table class='highchart tablesorter tablesorter-blue' id='top-ua' data-graph-type='pie' ".
     "data-graph-container='#top-ua'><thead>".
     "<tr><th>Useragent</th><th>Count</th></tr></thead>";
echo "<tbody>";

while ($row = $res->fetch_assoc()) {
  echo "<tr><td>",$row['useragent'],"</td><td>",$row['total'],"</td></tr>\n";
}
echo "</tbody></table>";
echo "</div></div>";


echo "<h3>Episode details</h3>";

$stm = "select episode, SUM(mp3_s) as mp3_s, SUM(mp3_d) as mp3_d, ".
         "SUM(aac_d) as aac_d, SUM(aac_s) as aac_s, SUM(opus_d) as opus_d, ".
         "SUM(opus_s) as opus_s,". 
         "SUM(mp3_s+mp3_d+aac_s+aac_d+opus_d+opus_s) as total ".
         "from downloads where podcast='".$podcast."' and date between '".$start."' and '".$end."'".
         " group by episode order by total DESC";

$res = $dbh->query($stm);

echo "<table id='ep-details' class='tablesorter tablesorter-blue'><thead>".
     "<tr><th>Episode</th><th>MP3 download</th><th>MP3 stream</th>".
     "<th>AAC download</th><th>AAC stream</th><th>OPUS download</th><th>OPUS stream</th><th>Total</th></tr></thead><tbody>";

while ($row = $res->fetch_assoc()) {
  echo "<tr><td>",$row['episode'],"</td><td>",$row['mp3_d'],"</td><td>",$row['mp3_s'],"</td><td>",$row['aac_d'],"</td><td>",$row['aac_s'],
     "</td><td>",$row['opus_d'],"</td><td>",$row['opus_s'],"</td><td>",$row['total'],"</td></tr>";
}

echo "</tbody></table>";
}


?>

</div>
</body></html>
