#!/bin/sh
 
if [ $# -gt 0 -a -d "$1" ]; then 
    export WORKDIR="$1"
else 
    export WORKDIR="/var/www/podseed.org/logs/"
fi

cat <<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us">
<head>
<link href="http://podseed.org/css/bootstrap.css" rel="stylesheet">
<link href="http://podseed.org/css/podseed.css" rel="stylesheet" type="text/css">
<div class="container">
<div class="row"><div class="col-lg-8">
<h1>Podseed statistics</h1>
EOF

echo "<ul>"

for file in $WORKDIR/stats*.html ; do 
    TITLE=$(grep h1 $file | perl -pe 's/<[^>]+>//g; chomp')
    bfile=$(basename $file)
    echo "<li><a href=\"${bfile}\">$TITLE</a><ul>"
    for link in $(grep h2 $file | cut -d\" -f6); do 
        echo "<li><a href=\"${bfile}#${link}\">"$(echo $link | sed 's/-stats//')"</a></li>"
    done
    echo "</ul>"
done

echo "</ul>"
cat <<EOF
</div>
</div>
</div>
</body>
</html>
EOF
