#!/usr/bin/perl 

use URI::Escape;
use DBI;
use DBD::mysql;

# use strict; 

sub lookup_month  {
    $lookup_table = {
        "Jan" => "01",
        "Feb" => "02",
        "Mar" => "03",
        "Apr" => "04",
        "May" => "05",
        "Jun" => "06",
        "Jul" => "07",
        "Aug" => "08",
        "Sep" => "09",
        "Oct" => "10",
        "Nov" => "11",
        "Dec" => "12"
    };
    return $lookup_table->{$_[0]};
};


sub log_ua {
    if ($ua =~ /(Googlebot|bingbot|spyder|MJ12bot|Bot|Slurp)/) {
    } elsif ($ua =~ /iTunes/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"iTunes"}++;
    } elsif ($ua =~ /EinschlafenPodcastAndroid/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Einschlafen Android App"}++;
    } elsif ($ua =~ /Instacast/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Instacast"}++;
    } elsif ($ua =~ /gPodder/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"gPodder"}++;
    } elsif ($ua =~ /(AppleCoreMedia|itunesstored)/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"iOS MediaPlayer"}++;
    } elsif ($ua =~ /Podkicker/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Podkicker"}++;
    } elsif ($ua =~ /PritTorrent/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"PritTorrent"}++;
    } elsif ($ua =~ /PodCat/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"PodCat"}++;
    } elsif ($ua =~ /Azureus/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Azureus"}++;
    } elsif ($ua =~ /FlipboardProxy/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"FlipBoard"}++;
    } elsif ($ua =~ /NSPlayer/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Windows Media Player"}++;
    } elsif ($ua =~ /icatcher/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"iCatcher!"}++;
    } elsif ($ua =~ /(RSSRadio|RSS_Radio)/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"RSSRadio"}++;
    } elsif ($ua =~ /Downcast/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Downcast"}++;
    } elsif ($ua =~ /Overcast/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Overcast"}++;
    } elsif ($ua =~ /AntennaPod/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"AntennaPod"}++;
    } elsif ($ua =~ /iCatcher/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"iCatcher"}++;
    } elsif ($ua =~ /Windows Phone/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Windows Phone"}++;
    } elsif ($ua =~ /Media Monkey/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Media Monkey"}++;
    } elsif ($ua =~ /DoggCatcher/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"DoggCatcher"}++;
    } elsif ($ua =~ /Miro/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Miro"}++;
    } elsif ($ua =~ /Wget/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"GNU wget"}++;
    } elsif ($ua =~ /foobar2000/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"foobar2000"}++;
    } elsif ($ua =~ /Castro/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Castro"}++;
    } elsif ($ua =~ /Winamp/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"WinAmp"}++;
    } elsif ($ua =~ /BeyondPod/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"BeyondPod"}++;
    } elsif ($ua =~ /^-$/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Unknown"}++;
    } elsif ($ua =~ /(Dalvik|Android|HTC|htc)/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"Android"}++;
    } elsif ($ua =~ /XBMC/) {
        $log->{$podcast}->{"useragent"}->{$date}->{"XBMC"}++;
    } else {
        $log->{$podcast}->{"useragent"}->{$date}->{$ua}++;
    }
}

while (<>) {
    if ( $_ =~ m@([^\s]+) - - \[(\d+)/([^\/]+)/(\d{4})[^\]]+\] "GET /(\w+)/(.+)\.([om][^\?]+)(\?.+)?\sHTTP[^\s]+\s(\d+)\s[\d\"-\s]+([^\"]+)\"$@ ) {
        $ip = $1;
        $day = $2;
        $month = lookup_month($3);
        $year = $4;
        $podcast = $5;
        $episode = uri_unescape($6);
        $format = $7;
        $status = $9;
        $ua = $10;
        $date = $year.'-'.$month.'-'.$day;

        if ( $format =~ m/m4[ab]/ ) {
            $format = "aac";
        }

        if ($status == "200") {
            $log->{$podcast}->{"episode"}->{$date}->{$episode}->{$format}->{"download"}++;
            &log_ua;
        } elsif ($status == "206") {
            $foo->{$date}->{$ip}++;
            if ($foo->{$date}->{$ip} == 1) {
                $log->{$podcast}->{"episode"}->{$date}->{$episode}->{$format}->{"stream"}++;
                &log_ua;
            }
        }
        
    }
}

$nowstring = localtime();

$dsn = "DBI:mysql:database=podseed;host=dbhost";
$dbh = DBI->connect($dsn, "dbuser", "dbpass");

# dbh->do.. 

foreach $podcast (sort(keys(%{$log}))) {
    # for each podcast 
    foreach $date (sort(keys(%{$log->{$podcast}->{"episode"}}))) {
        foreach $episode (sort(keys(%{$log->{$podcast}->{"episode"}->{$date}}))) {
            $mp3_d = $log->{$podcast}->{"episode"}->{$date}->{$episode}->{"mp3"}->{"download"} || "0"; 
            $mp3_s = $log->{$podcast}->{"episode"}->{$date}->{$episode}->{"mp3"}->{"stream"} || "0"; 
            $aac_d = $log->{$podcast}->{"episode"}->{$date}->{$episode}->{"aac"}->{"download"} || "0";
            $aac_s = $log->{$podcast}->{"episode"}->{$date}->{$episode}->{"aac"}->{"stream"} || "0" ;
            $opus_d = $log->{$podcast}->{"episode"}->{$date}->{$episode}->{"opus"}->{"download"} || "0";
            $opus_s = $log->{$podcast}->{"episode"}->{$date}->{$episode}->{"opus"}->{"stream"} || "0";
            $query = "INSERT INTO downloads VALUES " .
                     "('$date','$podcast','$episode'," . 
                     "'".$mp3_d."',".
                     "'".$mp3_s."',".
                     "'".$aac_d."',".
                     "'".$aac_s."',".
                     "'".$opus_d."',".
                     "'".$opus_s."') ".
                     " ON DUPLICATE KEY UPDATE mp3_d = mp3_d + VALUES(mp3_d),".
                     "mp3_s = mp3_s + VALUES(mp3_s),".
                     "aac_d = aac_d + VALUES(aac_d),".
                     "aac_s = aac_s + VALUES(aac_s),".
                     "opus_d = opus_d + VALUES(opus_d),".
                     "opus_s = opus_s + VALUES(opus_s)";
            $dbh->do($query);
        }
    }

    foreach $date (sort(keys(%{$log->{$podcast}->{"useragent"}}))) {
        foreach $agent (sort(keys(%{$log->{$podcast}->{"useragent"}->{$date}}))) {
            $count = $log->{$podcast}->{"useragent"}->{$date}->{$agent};
            $query = "INSERT INTO useragents VALUES " .
                     "('$date','$podcast'," . 
                     "'".$agent."',".
                     "'".$count."') ".
                     " ON DUPLICATE KEY UPDATE count = count + VALUES(count)";
            $dbh->do($query);
        }
    }

    
}


$dbh->disconnect();
