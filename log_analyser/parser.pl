#!/usr/bin/perl 

use URI::Escape;

sub log_ua {
    if ($ua =~ /(Googlebot|bingbot|spyder|MJ12bot)/) {
    } elsif ($ua =~ /iTunes/) {
        $log->{$podcast}->{"useragent"}->{"iTunes"}++;
    } elsif ($ua =~ /(Dalvik|Android|HTC|htc)/) {
        $log->{$podcast}->{"useragent"}->{"Android"}++;
    } elsif ($ua =~ /Instacast/) {
        $log->{$podcast}->{"useragent"}->{"Instacast"}++;
    } elsif ($ua =~ /gPodder/) {
        $log->{$podcast}->{"useragent"}->{"gPodder"}++;
    } elsif ($ua =~ /(AppleCoreMedia|itunesstored)/) {
        $log->{$podcast}->{"useragent"}->{"iOS MediaPlayer"}++;
    } elsif ($ua =~ /Podkicker/) {
        $log->{$podcast}->{"useragent"}->{"Podkicker"}++;
    } elsif ($ua =~ /PritTorrent/) {
        $log->{$podcast}->{"useragent"}->{"PritTorrent"}++;
    } elsif ($ua =~ /FlipboardProxy/) {
        $log->{$podcast}->{"useragent"}->{"FlipBoard"}++;
    } elsif ($ua =~ /NSPlayer/) {
        $log->{$podcast}->{"useragent"}->{"Windows Media Player"}++;
    } elsif ($ua =~ /icatcher/) {
        $log->{$podcast}->{"useragent"}->{"iCatcher!"}++;
    } elsif ($ua =~ /(RSSRadio|RSS_Radio)/) {
        $log->{$podcast}->{"useragent"}->{"RSSRadio"}++;
    } elsif ($ua =~ /Downcast/) {
        $log->{$podcast}->{"useragent"}->{"Downcast"}++;
    } elsif ($ua =~ /Miro/) {
        $log->{$podcast}->{"useragent"}->{"Miro"}++;
    } elsif ($ua =~ /Wget/) {
        $log->{$podcast}->{"useragent"}->{"GNU wget"}++;
    } elsif ($ua =~ /foobar2000/) {
        $log->{$podcast}->{"useragent"}->{"foobar2000"}++;
    } elsif ($ua =~ /Castro/) {
        $log->{$podcast}->{"useragent"}->{"Castro"}++;
    } elsif ($ua =~ /Winamp/) {
        $log->{$podcast}->{"useragent"}->{"WinAmp"}++;
    } else {
        $log->{$podcast}->{"useragent"}->{$ua}++;
    }
}

while (<>) {
    if ( $_ =~ m@([^\s]+) - - \[(\d+)/([^\/]+)/(\d{4})[^\]]+\] "GET /(\w+)/(.+)\.([om][^\?]+)(\?.+)?\sHTTP[^\s]+\s(\d+)\s[\d\"-\s]+([^\"]+)\"$@ ) {
        $ip = $1;
        $day = $2;
        $month = $3;
        $year = $4;
        $podcast = $5;
        $episode = uri_unescape($6);
        $format = $7;
        $status = $9;
        $ua = $10;

        if ( $format =~ m/m4[ab]/ ) {
            $format = "aac";
        }

        if ($status == "200") {
            $log->{$podcast}->{"episode"}->{$episode}->{$format}->{"download"}++;
            # $log->{"useragent"}->{$ua}++;
            &log_ua;
        } elsif ($status == "206") {
            $foo->{$day}->{$ip}++;
            
            # only log it if we haven't seen the IP 
            
            if ($foo->{$day}->{$ip} == 1) {
                $log->{$podcast}->{"episode"}->{$episode}->{$format}->{"stream"}++;
                &log_ua;
            }
        }
    }
}

$nowstring = localtime();

# print html header

print <<EOF;
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us">
<head>
<link href="http://podseed.org/css/bootstrap.css" rel="stylesheet">
<link href="http://podseed.org/css/podseed.css" rel="stylesheet" type="text/css">
<link href="theme.blue.css" rel="stylesheet">
<script type="text/javascript" src="jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="jquery.tablesorter.widgets.min.js"></script>
EOF

print "<script type=\"text/javascript\">\n";
print '$(function() {'."\n";
foreach (keys(%{$log})) {
    print '$("#',$_,'").tablesorter({theme:\'blue\', sortList: [[1,1]]});'."\n";
    print '$("#',$_,'-ua").tablesorter({theme:\'blue\', sortList: [[1,1]]});'."\n";
}
print '});'."\n</script>";
print "</head>\n<body><div class=\"container\">\n";
print "<a name=\"top\"><h1>Podcast Statistics $month $year</h1></a>\n";
print "<p>Last Update $nowstring</p>\n";
print "<ul>\n";

foreach (sort(keys(%{$log}))) {
    print "<li>$_<ul>\n";
    print "<li><a href=\"#$_-stats\">Access statistics</a></li>\n";
    print "<li><a href=\"#$_-ua\">Useragent statistics</a></li>\n";
    print "</ul></li>\n";
}

print "</ul>\n";

# render table

foreach (sort(keys(%{$log}))) {
    print '<div class="row"><div class="col-lg-8">';
    print "<a name=\"$_-stats\"><h2>$_</h2></a>\n";
    print "<h3>Access statistics</h3>\n";
    # write table header
    print "<table id=\"$_\" class=\"tablesorter\"><thead><tr>" ,
        "<th>Episode</th><th>MP3 download</th><th>MP3 stream</th><th>AAC download</th><th>AAC stream</th><th>OPUS download</th><th>OPUS stream</th><th>total download</th><th>total stream</th></tr></thead><tbody>\n";
    $ref = $log->{$_}->{"episode"};
    $total_stream = 0;
    $total_download = 0;
    foreach (sort(keys(%{$ref}))) {
        $sum_stream = $ref->{$_}->{"opus"}->{"stream"} + $ref->{$_}->{"aac"}->{"stream"} + $ref->{$_}->{"mp3"}->{"stream"};
        $sum_download = $ref->{$_}->{"opus"}->{"download"} + $ref->{$_}->{"aac"}->{"download"} + $ref->{$_}->{"mp3"}->{"download"};
        $total_stream = $total_stream + $sum_stream;
        $total_download = $total_download + $sum_download;
        print "<tr><td>".$_."</td>" ,
            "<td>".$ref->{$_}->{"mp3"}->{"download"}."</td>",
            "<td>".$ref->{$_}->{"mp3"}->{"stream"}."</td>",
            "<td>".$ref->{$_}->{"aac"}->{"download"}."</td>",
            "<td>".$ref->{$_}->{"aac"}->{"stream"}."</td>",
            "<td>".$ref->{$_}->{"opus"}->{"download"}."</td>",
            "<td>".$ref->{$_}->{"opus"}->{"stream"}."</td>",
            "<td>".$sum_download."</td>",
            "<td>".$sum_stream."</td>",
        "</tr>\n";
    }
    print "</tbody></table>\n";
    print "<table id=\"$_-sum\" class=\"tablesorter tablesorter-blue\"><thead>", 
          "<tr>","<th>&nbsp;</th><th>Download</th><th>Stream</th></tr></thead><tbody><tr>";
    print "<td>Total</td><td>".$total_download."</td><td>".$total_stream."</td></tr>\n";
    print "</tbody></table>\n<a href=\"#top\">Top</a>\n";
    print "<h3><a name=\"$_-ua\">Useragents</a></h3>\n";
    print "<table id=\"$_-ua\" class=\"tablesorter\"><thead><tr>" ,
        "<th>Useragent</th><th>Count</th></tr></thead><tbody>\n";
    $ref = $log->{$_}->{"useragent"};
    foreach (sort(keys(%{$ref}))) {
        print "<tr><td>".$_."</td><td>".$ref->{$_}."</td></tr>\n";
    }
    print "</tbody></table>\n<a href=\"#top\">Top</a></div></div>\n";

}

print "</div></body></html>\n";
